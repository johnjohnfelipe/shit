﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTableScheduler
{

    //public class EmployeeCollection
    //{
    //    public static List<Employee> SortByLeastWorkload(List<Employee> empList)
    //    {
    //        return empList.OrderBy(o => o.AssignedWorkDay).ToList();
    //    }

    //}


    /// <summary>
    /// Employee data model
    /// </summary>
    public class Employee
    {
        public HashSet<uint> Leaves = new HashSet<uint>();
        public HashSet<uint> WorkingDays = new HashSet<uint>();
        public String Name = "";

        uint _totalWorkingDay = 0;
        uint _maxWorkingDay = uint.MaxValue;
        public uint TotalWorkingDay { get { return _totalWorkingDay; } set { _totalWorkingDay = value; } }
        public uint MaxWorkingDay { get { return _maxWorkingDay; } set { _maxWorkingDay = value; } }



    }
}
