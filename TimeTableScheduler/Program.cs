﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace TimeTableScheduler
{


    class Program
    {
        static void Main(string[] args)
        {
            //testing
            TimeTable tt = new TimeTable(30, new string[] { "col1", "col2" });

            EmployeePool empPool = new EmployeePool();
            empPool.Add(new Employee() { Name = "a", Leaves = new HashSet<uint>( new uint[] {1}) });
            empPool.Add(new Employee() { Name = "b", Leaves = new HashSet<uint>(new uint[] { 2 }) });
            empPool.Add(new Employee() { Name = "c", Leaves = new HashSet<uint>(new uint[] {3 }) });
            empPool.Add(new Employee() { Name = "d", Leaves = new HashSet<uint>(new uint[] {  }) });
            empPool.Add(new Employee() { Name = "e", Leaves = new HashSet<uint>(new uint[] {  }) });
            empPool.Add(new Employee() { Name = "f", Leaves = new HashSet<uint>(new uint[] { }) });
            empPool.Add(new Employee() { Name = "g", Leaves = new HashSet<uint>(new uint[] { }) });

            uint totalDaySchedules = 10;

            Dictionary<uint, Employee> scheduledList = new Dictionary<uint, Employee>();
            DateTime startDate = new DateTime(2015, 8, 1);
            for (uint day = 1; day <= tt.Days; day++)
            {
                Employee emp = empPool.ChooseEmployee(startDate, day, "col1", tt);
                if (emp != null)
                {
                    tt.SetEmployee(day, "col1", emp);
                    emp.TotalWorkingDay++;
                    emp.WorkingDays.Add(day);
                }
                Debug.WriteLine(String.Format("{0} Day: {1} Emp: {2}", "col1", day, emp==null? "":emp.Name));
            }
            for (uint day = 1; day <= tt.Days; day++)
            {
                Employee emp = empPool.ChooseEmployee(startDate, day, "col2", tt);
                if (emp != null)
                {
                    tt.SetEmployee(day, "col2", emp);
                    emp.TotalWorkingDay++;
                    emp.WorkingDays.Add(day);

                }
                Debug.WriteLine(String.Format("{0} Day: {1} Emp: {2}", "col2", day, emp == null ? "" : emp.Name));
            }


            //uint[] emptyDays = tt.GetEmptySlotByColume("c1");

            //Random rnd = new Random();
            //for (int i = 0; i < 100; i++)
            //{
            //    int num = rnd.Next(1, 3);
            //    Debug.WriteLine("random:" + num);
            //}

            
            

        }
    }
}
